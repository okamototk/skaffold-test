# Build Container
FROM golang:latest as builder
WORKDIR /go/src/gitlab.com/okamototk/hello
COPY app .
# Set Environment Variable
ENV CGO_ENABLED=0
ENV GOOS=linux
ENV GOARCH=amd64
# Build
RUN go build -o hello hello.go

# Runtime Container
FROM alpine
COPY --from=builder /go/src/gitlab.com/okamototk/hello/hello /hello
EXPOSE 80
ENTRYPOINT ["/hello"]
